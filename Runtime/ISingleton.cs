namespace Garra.Utils
{
    public interface ISingleton
    {
        /// <summary>
        /// Creation Date:   
        /// Product Name:    Garra Utils
        /// Developers:      Garra
        /// Product Name:    Garra Utils
        /// Description:     
        /// </summary>
        void OnDynamicCreation();
    }
}