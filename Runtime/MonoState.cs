using System.Collections.Generic;
using UnityEngine;

namespace Garra.Utils
{
    public class MonoState : MonoBehaviour
    {
    public static List<string> Log = new List<string>();

    protected static void SetLog(string value, string field, string setter)
    {
        Log.Add($"Value set: ${value}, in to field: ${field}, by ${setter}");
    }
    }
}