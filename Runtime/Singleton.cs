using UnityEngine;

namespace Garra.Utils
{
	public abstract class Singleton<T> : MonoBehaviour, ISingleton where T : MonoBehaviour, ISingleton
	{
		/// <summary>
		/// Creation Date:   13/05/2020 15:22:35
		/// Product Name:    Garra Utils
		/// Developers:      Jon Kenkel (nonathaj), FDT Dev, Garra
		/// Company:         Garra
		/// Description:	 Singleton Class.
		/// 				 As a note, this is made as MonoBehaviour because we need Coroutines.
		/// 				 original version from https://wiki.unity3d.com/index.php/Secure_UnitySingleton
		/// Changelog:		 --/--/-- - Fixed problem when a singleton instance exists in a scene, but nobody used
		///					   		    it's instance yet, and something checks if it exists by using the method Exists
		/// </summary>
		private static T _instance;

		private static object _lock = new object();

		public static bool Exists
		{
			get { return _instance != null; }
		}

		protected void Awake()
		{
			if (Exists && _instance != null && _instance != this)
				Destroy(gameObject);
			if (_instance == null)
				_instance = this as T;
			OnAwake();
		}

		protected virtual void OnAwake()
		{

		}

		/// <summary>
		/// Destroy the current static instance of this singleton
		/// </summary>
		/// <param name="destroyGameObject">Should we destroy the gameobject of the instance too?</param>
		public static void DestroyInstance(bool destroyGameObject = true)
		{
			if (Exists)
			{
				if (destroyGameObject)
					Destroy(_instance.gameObject);
				else
					Destroy(_instance);
				_instance = null;
			}
		}

		/// <summary>
		/// Returns an instance of this singleton
		/// </summary>
		public static T Instance
		{
			get
			{
				return _instance;
			}
		}

		public virtual void OnDynamicCreation()
		{

		}

		private static bool applicationIsQuitting = false;

		/// <summary>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed, 
		///   it will create a buggy ghost object that will stay on the Editor scene
		///   even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </summary>
		public void OnDestroy()
		{
			applicationIsQuitting = true;
		}
	}
}
